#!/usr/bin/env sh

#Simple script to list IP addresses on active interfaces
while read -r INTERFACE;
do
  if [ "${INTERFACE}" != 'lo' ]
    then
      echo "${INTERFACE}: $(ip a  show wlp3s0 | grep inet | head -1 | awk '{print $2}')"
  fi
done <<< "$(ip link show | grep -vE DOWN | grep -E '^[1-9]' | awk '{print $2}' | tr -d ':')"
