#!/usr/bin/env sh

#Pull and install the latest version of the Docker compose. If installed updates it.

# Make sure the script is being executed with superuser privileges.
if [ "$(id -u)" -ne 0 ]
then
    echo "Script needs to be run as root!"
    exit 1
else
    #Check if Docker Compose is installed
    if [ -f '/usr/local/bin/docker-compose' ]
    then
        CUR_VERSION=$(docker-compose --version | cut -d ' ' -f 3 | sed 's/,//')
    else
        CUR_VERSION=0
    fi

    #Check latest avaliable version
    URL=$(curl -s https://github.com/docker/compose/releases/latest)
    URL=${URL#*'"'}
    URL=${URL%'"'*}
    NEW_VERSION=$(echo "${URL}" | cut -d '/' -f8)

    #Check if latest and installed version are different and install latest. If not present also install latest.
    if [ "${CUR_VERSION}" = "${NEW_VERSION}" ]
    then
        echo "Latest version is installed: ${CUR_VERSION}"
        exit 0
    else
        echo "Installing Docker Compose version ${NEW_VERSION}"
        curl -s -L "https://github.com/docker/compose/releases/download/${NEW_VERSION}/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
        chmod +x /usr/local/bin/docker-compose
        exit 0
    fi
fi
