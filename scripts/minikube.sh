#!/bin/env bash

# Minikube update script file
# Checks if minikube is installed.
# If yes checks if an update is available and can install it.
# If not can install it.
# Works for amd64 and aarch64

# To do
# Test on more systems

# function
install_latest_minikube()
{
    read -p "Install it? (latest version will be installed)?(y/n) " ANSR
    case ${ANSR} in
    [yY][eE][sS]|[yY])
        # GitHub repository URL
        repo_url="https://api.github.com/repos/kubernetes/minikube/releases/latest"
        # Send a GET request to the GitHub API and store the response in a variable
        response=$(curl -s $repo_url)
        # Extract the "tag_name" field from the response using grep and awk
        latest_tag=$(echo $response | grep -o '"tag_name": ".*"' | awk -F'"' '{print $4}')
        # Print the latest release tag
        # echo "Latest release tag: $latest_tag"

        # minikube delete
        if [ -f "$(which minikube)" ];then
            sudo rm -rf /usr/local/bin/minikube
        fi

        if [ $(arch) == "aarch64" ]; then
            sudo curl -Lo minikube https://storage.googleapis.com/minikube/releases/latest/minikube-linux-arm64
        else
            sudo curl -Lo minikube https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64
        fi

        sudo chmod +x minikube && sudo mv minikube /usr/local/bin/
        echo "Minikube installed: $(minikube version | head -1 | awk '{print $3}')"
        ;;
    *)
        echo "Minikube will not be installed."
        ;;
    esac

}

# Check if minikube is installed
if [ ! -f "$(which minikube)" ];then
    echo "Minikube not present."
    install_latest_minikube
else
    # Currently installed version
    echo "Minikube version check"
    currentversion=$(minikube update-check | head -1 | awk '{print $2}')
    latestversion=$(minikube update-check | tail -1 | awk '{print $2}')

    if [ "${currentversion}" == "${latestversion}" ]; then
        echo "Minikube is in the latest version: ${currentversion}"
    else
        echo "Newer version available: ${latestversion}"
        install_latest_minikube
    fi
fi
