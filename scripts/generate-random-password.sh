#!/usr/bin/env sh
#Generates random password.
#Scritps uses getopts to process provided options and shift to given arguments.

###Funcions
display_usage(){
 #Function displays usage of the scripts
 echo "Usage ${0} [-sv] [-l LENGHT] [USERNAME]" >&2
 echo "Generate random password."
 echo "  -l LENGHT Specify the password lenght."
 echo "  -s        Append special character to the password."
 echo "  -v        Enable verbose mode."
}

verbose(){
 #Functions checks if the -v option was given and if yes display all messages.
 if [ "${VERBOSE}" = "yes" ]
    then
      echo "${1}"
 fi
}

generate_password(){
 #Function generates password with lenght specified by user. If not uses default.
 #If special character option was selected it adds it at the end of the password.
 if [ "${LENGHT}" ]
    then
        if [ "${SPECIAL_CHAR}" = "yes" ]
           then
               PASS_LENGHT=$(( LENGHT-1 ))
           else
               PASS_LENGHT=${LENGHT}
        fi
 else
        PASS_LENGHT=${DEFAULT_LENGHT}
 fi

 if [ "${SPECIAL_CHAR}" = "yes" ]
    then
        TEMP_PASS=$(date +"%d%N" | sha256sum | head -c "${PASS_LENGHT}")
        RANDOM_CHAR=$(tr -dc '@#$%^&*()!+_' < /dev/urandom| fold -w 1 | head -n 1)
        TEMP_PASS=${TEMP_PASS}${RANDOM_CHAR}
        RESULT_PASS=$(echo "${TEMP_PASS}" | fold -w 1 | shuf | xargs | sed "s/ //g")
 else
     RESULT_PASS=$(date +"%d%N" | sha256sum | head -c "${PASS_LENGHT}")
 fi
}
###

###Variables
DEFAULT_LENGHT=32

#Read and process options
while getopts sl:v OPTION
do
  case ${OPTION} in
       v)
         VERBOSE='yes'
         ;;
       s)
         SPECIAL_CHAR='yes'
         ;;
       l)
        LENGHT=${OPTARG}
        ;;
       ?)
        display_usage
        ;;
  esac
done

#Removing options while leaving all provided arguments
shift "$(( OPTIND -1 ))"

verbose "Generates password"
echo "Password for user: ${*}"
generate_password
echo "${RESULT_PASS}"

exit 0
