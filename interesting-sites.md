# Sites so good they need to be not only in the bookmarks

- [**commandlinefu**](https://www.commandlinefu.com/) - The place to record those command-line gems that you return to again and again
- [**ArchWiki**](https://wiki.archlinux.org/) - The best Linux related wiki by far
- [**The Debian Administrator's Handbook**](https://debian-handbook.info/browse/stable/) - Debian administrator's guide
- [**GoLinuxCloud**](https://www.golinuxcloud.com/) - Few interesting tutorials (yet to check)
- [**The POSIX Shell And Utilities**](https://shellhaters.org/)
- [**Comprehensive Linux Cheatsheet**](https://gto76.github.io/linux-cheatsheet/)
- [**Systemd based Linux file-hierarchy**](https://www.freedesktop.org/software/systemd/man/file-hierarchy.html)
- [**Rosetta Code**](https://rosettacode.org/wiki/Rosetta_Code) - Rosetta Code is a programming chrestomathy site. The idea is to present solutions to the same task in as many different languages as possible
- [**ZSH shell manual**](http://zsh.sourceforge.net/Doc/Release/index.html#Top)
- [**Modern Unix tools replacement**](https://github.com/ibraheemdev/modern-unix)
- [**Interactive git tutorial with labs**](https://learngitbranching.js.org/?locale=en)
