# What is what

- tech-notes - notes on varied Open Source tools/technologies (WiP)
- dot-files - customized configuration files for tools I use
- me-tools - list of some useful tools
- scripts - scripting playground
- ansible - Ansible playground
- ansible/ansible-lab - simple, Docker based lab for Ansible
- interesting-sites - websites with interesting technical content
