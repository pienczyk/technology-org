# Start ssh-agent on login

**On systemd based OS create a new unit** <br>
`sudo vim /etc/systemd/user/ssh-agent.service`

```
[Unit]
Description=SSH key agent

[Service]
Type=simple
Environment=SSH_AUTH_SOCK=%t/ssh-agent.socket
ExecStart=/usr/bin/ssh-agent -D -a $SSH_AUTH_SOCK

[Install]
WantedBy=default.target
```

Enable and start system <br>
`systemctl --user enable ssh-agent.service` <br>
`systemctl --user start ssh-agent.service` <br>

Add to ssh config <br>
`AddKeysToAgent  yes`

Key will be added when first used. <br>
**To verify keys in the agent** <br>
`ssh-add [-L, -l]` <br>

**To add a key manually** <br>
`ssh-add PATH_TO_A_KEY` <br>
