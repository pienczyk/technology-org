# [tcpdump](https://www.tcpdump.org/) - a powerful command-line packet analyzer

- tcpdump -D - See the list of interfaces on which tcpdump can listen:
- tcpdump -i eth0 - Listen on interface eth0:
- tcpdump -i any - Listen on any available interface (cannot be done in promiscuous mode. Requires Linux kernel 2.2 or greater):
- tcpdump -v - Be verbose while capturing packets:
- tcpdump -vv - Be more verbose while capturing packets:
- tcpdump -vvv - Be very verbose while capturing packets:
- tcpdump -v -X - Be verbose and print the data of each packet in both hex and ASCII, excluding the link level header:
- tcpdump -v -XX - Be verbose and print the data of each packet in both hex and ASCII, also including the link level header:
- tcpdump -q - Be less verbose (than the default) while capturing packets:
- tcpdump -c 100 - Limit the capture to 100 packets:
- tcpdump -w capture.cap - Record the packet capture to a file called capture.cap:
- tcpdump -v -w capture.cap - Record the packet capture to a file called capture.cap but display on-screen how many packets have been captured in real-time:
- tcpdump -r capture.cap - Display the packets of a file called capture.cap:
- tcpdump -vvv -r capture.cap - Display the packets using maximum detail of a file called capture.cap:
- tcpdump -n (-nn) - Display IP addresses and port numbers instead of domain and service names when capturing packets
- tcpdump -n dst host 192.168.1.1 - Capture any packets where the destination host is 192.168.1.1. Display IP addresses and port numbers:
- tcpdump -n src host 192.168.1.1 - Capture any packets where the source host is 192.168.1.1. Display IP addresses and port numbers:
- tcpdump -n host 192.168.1.1 - Capture any packets where the source or destination host is 192.168.1.1. Display IP addresses and port numbers:
- tcpdump -n dst net 192.168.1.0/24 - Capture any packets where the destination network is 192.168.1.0/24. Display IP addresses and port numbers:
- tcpdump -n src net 192.168.1.0/24 - Capture any packets where the source network is 192.168.1.0/24. Display IP addresses and port numbers:
- tcpdump -n net 192.168.1.0/24 - Capture any packets where the source or destination network is 192.168.1.0/24. Display IP addresses and port numbers:
- tcpdump -n dst port 23 - Capture any packets where the destination port is 23. Display IP addresses and port numbers:
- tcpdump -n dst portrange 1-1023 - Capture any packets where the destination port is is between 1 and 1023 inclusive. Display IP addresses and port numbers:
- tcpdump -n tcp dst portrange 1-1023 - Capture only TCP packets where the destination port is is between 1 and 1023 inclusive. Display IP addresses and port numbers:
- tcpdump -n udp dst portrange 1-1023 - Capture only UDP packets where the destination port is is between 1 and 1023 inclusive. Display IP addresses and port numbers:
- tcpdump -v icmp - Capture any ICMP packets:
- tcpdump -v arp - Capture any ARP packets:
- tcpdump -v "icmp or arp" - Capture either ICMP or ARP packets:
- tcpdump -n "broadcast or multicast" - Capture any packets that are broadcast or multicast:
- tcpdump -s 500 - Capture 500 bytes of data for each packet rather than the default of 68 bytes:
- tcpdump -s 0 - Capture all bytes of data within the packet:
- tcpdump -n "dst host 192.168.1.1 and dst port 23" - Capture any packets with destination IP 192.168.1.1 and destination port 23. Display IP addresses and port numbers:
- tcpdump -n "dst host 192.168.1.1 and (dst port 80 or dst port 443)" - Capture any packets with destination IP 192.168.1.1 and destination port 80 or 443.

### Filtering output:

tcpdump -i eth0 port not 8080 - exclude port 8080
