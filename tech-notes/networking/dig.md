[dig](https://linux.die.net/man/1/dig)

**Query Domain “A” Record** </br>
dig DOMAIN_TO_CHECK

**Query Domain “A” Record with +short** </br>
dig DOMAIN_TO_CHECK +short

**Querying MX Record for Domain** </br>
dig DOMAIN_TO_CHECK MX

**DNS Reverse Look-up** </br>
dig -x IP_TO_RESOLVE +short

**Querying TTL (Time To Live) Record for Domain** </br>
dig DOMAIN_TO_CHECK TTL

**Querying Only Answer Section** </br>
dig DOMAIN_TO_CHECK +nocomments +noquestion +noauthority +noadditional +nostats

**Querying ALL DNS Records Types** </br>
dig DOMAIN_TO_CHECK ANY +noall +answer

**Querying Multiple DNS Records** </br>
dig DOMAIN_TO_CHECK_1 +noall +answer DOMAIN_TO_CHECK_2 ns +noall +answer

To reconfigure default response for searches a ${HOME}/.digrc file can be used. Required switches can be just added to the file.
