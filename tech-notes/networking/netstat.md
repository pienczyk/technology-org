**Listing all the LISTENING Ports of TCP and UDP Connections** </br>
netstat -a

**Listing [TCP,UDP] Ports connections** </br>
netstat -a[t,u]

**Listing all active listening ports connections** </br>
netstat -l

**Listing all active listening [TCP,UDP,UNIX]** </br>
netstat -l[t,u,x]

**Show statistics [PROTOCOL]** </br>
netstat -s[t,u]

**Displaying service name with their PID number** </br>
netstat -tp

**Displaying Promiscuous Mode** </br>
netstat -ac [REFRESH TIME]

**Displaying Kernel IP routing** </br>
netstat -r

**Showing network interface packet transactions including both transferring and receiving packets with MTU size** </br>
netstat -i

**Showing Kernel interface table (ifconfig like)** </br>
netstat -ie

**Displays multicast group membership information for both IPv4 and IPv6** </br>
netstat -g

**Print Netstat Information Continuously** </br>
netstat -c

**Finding Listening applications** </br>
netstat -ap [| grep APPLICATION]

**Displaying RAW Network Statistics** </br>
netstat --statistics --raw
