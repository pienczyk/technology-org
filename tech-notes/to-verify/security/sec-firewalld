FirewallD provides a dynamically managed firewall with support for network/firewall zones to define the trust level of network connections or interfaces.
A network zone defines the level of trust for network connections. This is a one to many relation, which means that a connection can only be part of one zone, but a zone can be used for many network connections.

These are the zones provided by firewalld sorted according to the default trust level of the zones from untrusted to trusted:

drop	 - Any incoming network packets are dropped, there is no reply. Only outgoing network connections are possible.
block	 - Any incoming network connections are rejected with an icmp-host-prohibited message for IPv4 and icmp6-adm-prohibited for IPv6. Only network connections initiated        within this system are possible.
public   - For use in public areas. You do not trust the other computers on networks to not harm your computer. Only selected incoming connections are accepted.
external - For use on external networks with masquerading enabled especially for routers. You do not trust the other computers on networks to not harm your computer. Only         selected incoming connections are accepted.
dmz		 - For computers in your demilitarized zone that are publicly-accessible with limited access to your internal network. Only selected incoming connections are accepted.
work	 - For use in work areas. You mostly trust the other computers on networks to not harm your computer. Only selected incoming connections are accepted.
home	 - For use in home areas. You mostly trust the other computers on networks to not harm your computer. Only selected incoming connections are accepted.
internal - For use on internal networks. You mostly trust the other computers on the networks to not harm your computer. Only selected incoming connections are accepted.
trusted  - All network connections are accepted.


firewall-cmd --state											- status of firewalld
firewall-cmd --reload											- reload the firewall without loosing state information
firewall-cmd --get-zones										- list of all supported zones
firewall-cmd --get-default-zone                                 - show default zone
firewall-cmd --get-services										- list of all supported services
firewall-cmd --get-icmptypes									- list of all supported icmptypes
firewall-cmd --list-services                                    - list currently configured services
firewall-cmd --list-all-zones									- list all zones with the enabled features
firewall-cmd [--zone=<zone>] --list-all							- print zone <zone> with the enabled features. If zone is omitted, the default zone will be used
firewall-cmd --get-default-zone									- the default zone set for network connections
firewall-cmd --set-default-zone=<zone>							- set the default zone
firewall-cmd --get-active-zones									- active zones
firewall-cmd --get-zone-of-interface=<interface>				- get zone related to an interface
firewall-cmd [--zone=<zone>] --add-interface=<interface>		- add an interface to a zone, if it was not in a zone before. If the zone options is omitted, the default zone will be used. The interfaces are reapplied after reloads.
firewall-cmd [--zone=<zone>] --change-interface=<interface>		- change the zone an interface belongs to
firewall-cmd [--zone=<zone>] --remove-interface=<interface>		- remove an interface from a zone
firewall-cmd [--zone=<zone>] --query-interface=<interface>		- query if an interface is in a zone
firewall-cmd [ --zone=<zone> ] --list-services					- list the enabled services in a zone
firewall-cmd --panic-on 										- enable panic mode to block all network traffic in case of emergency
firewall-cmd --panic-off										- disable panic mode

Runtime zone handling
In the runtime mode the changes to zones are not permanent. The changes will be gone after reload or restart.

firewall-cmd [--zone=<zone>] --add-service=<service> [--timeout=<seconds>]					- enable a service in a zone
firewall-cmd [--zone=<zone>] --remove-service=<service>										- disable a service in a zone
firewall-cmd [--zone=<zone>] --query-service=<service>										- query if a service is enabled in a zone
firewall-cmd [--zone=<zone>] --add-port=<port>[-<port>]/<protocol> [--timeout=<seconds>]	- enable a port and protocol combination in a zone
firewall-cmd [--zone=<zone>] --remove-port=<port>[-<port>]/<protocol>						- disable a port and protocol combination in a zone
firewall-cmd [--zone=<zone>] --query-port=<port>[-<port>]/<protocol>						- query if a port and protocol combination in enabled in a zone
firewall-cmd [--zone=<zone>] --add-icmp-block=<icmptype>									- enable ICMP blocks in a zone
firewall-cmd [--zone=<zone>] --remove-icmp-block=<icmptype>									- disable ICMP blocks in a zone

Enabling/disabling and quering port forwarding in the zone:
firewall-cmd [--zone=<zone>] --add-forward-port=port=<port>[-<port>]:proto=<protocol> { :toport=<port>[-<port>] | :toaddr=<address> | :toport=<port>[-<port>]:toaddr=<address> }
firewall-cmd [--zone=<zone>] --remove-forward-port=port=<port>[-<port>]:proto=<protocol> { :toport=<port>[-<port>] | :toaddr=<address> | :toport=<port>[-<port>]:toaddr=<address>
firewall-cmd [--zone=<zone>] --query-forward-port=port=<port>[-<port>]:proto=<protocol> { :toport=<port>[-<port>] | :toaddr=<address> | :toport=<port>[-<port>]:toaddr=<address> }

Permanent zone handling
The permanent options are not affecting runtime directly. These options are only available after a reload or restart.
To have runtime and permanent setting, you need to supply both. The --permanent option needs to be the first option for all permanent calls.

Using static firewall rules with the iptables and ip6tables services

To use static firewall rules with the iptables and ip6tables services, install iptables-services and disable firewalld and enable iptables and ip6tables:
yum install iptables-services
systemctl mask firewalld.service
systemctl enable iptables.service
systemctl enable ip6tables.service
