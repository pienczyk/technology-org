# DNF - Dandified YUM

[dnf documentation](http://dnf.readthedocs.io/en/latest/command_ref.html)

**Install packege** </br>
dnf install PACKAGE_NAME

**Search for package** </br>
dnf search PACKAGE_NAME

**Check and install updates** </br>
dnf upgrade

**Remove packages** </br>
dnf remove PACKAGE_NAME

**Remove the cached packages from the system** </br>
dnf clean packages

**Remove the cache files generated from the repository metadata, remove the local cookie files, remove any cache repository metadata, and any cached packages from the system.** </br>
dnf clean all

**What package the file came from** </br>
dnf provides /usr/bin/less

**Lists description and summary information about installed and available packages** </br>
dnf info PACKAGE_NAME
