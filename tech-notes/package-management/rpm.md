# rpm - Red Hat Package Manager

**Querying and verifying packages** </br>
rpm {-q|--query} [select-options] [query-options]
rpm {-V|--verify} [select-options] [verify-options]
rpm --import PUBKEY ...
rpm {-K|--checksig} [--nosignature] [--nodigest] PACKAGE_FILE

**Installing, upgrading, and removing packages** </br>
rpm {-i|--install} [install-options] PACKAGE_FILE ...
rpm {-U|--upgrade} [install-options] PACKAGE_FILE ...
rpm {-F|--freshen} [install-options] PACKAGE_FILE ...
rpm {-e|--erase} [--allmatches] [--nodeps] [--noscripts] [--notriggers] [--test] PACKAGE_NAME ...

**Checking md5sum of the package_name** </br>
rpm -K --nosignature package_name

**List of all files installed by package** </br>
rpm -ql package_name

**Package_name dependencies** </br>
rpm -qR package_name

**Uninstall package_name** </br>
rpm -e package_name

**Upgrade installed package and install if not present** </br>
rpm -Uvh package_name

**Upgrade only installed package** </br>
rpm -Fvh package_name
