# YUM (Yellowdog Updater Modified)

main config file
/etc/yum.conf

plugins' config files
/etc/yum/pluginconf.d/

yum [options] [command] [package ...]

**Options:** </br>

- -c - specifies the config file location
- -q - run without output
- -y - assume that the answer to any question which would be asked is yes
- -v - run with a lot of debugging output

**Commands:** </br>

- clean [options] - expire-cache, packages, headers, metadata, dbcache, rpmdb, plugins, all
- erase/remove - used to remove the specified packages from the system as well as removing any packages which depend on the package being removed
- grouplist - list the available groups from all yum repos
- groupinfo - give the description and package list of a group
- groupinstall - install all of the individual packages in a group
- groupremove - remove all of the packages in a group
- info - list a description and summary information about available packages
- install - install the latest version of a package or group of packages while ensuring that all dependencies are satisfied
- search - find packages when you know something about the package but aren’t sure of it’s name
- search all - search for packages using not only names and descriptions
- update - update package; if run without any packages will update every currently installed package
- check-update - check for updates
- list updates - check for updates
- list installed - list installed packages
- list available - list of available packages
- repolist (-v) - list of configured repositories; add -v for verbose
- provides file_name - find the package providing the file
- history list - allows the user to view what has happened in past transactions
- yum-config-manager - display the current values of global Yum options (that is, the options specified in the [main] section of the /etc/yum.conf file)
- yum-complete-transaction - finds incomplete or aborted yum transactions on a system and attempts to complete them
- repoquery -l (--list) package_name - list of all files contained in the package package_name

**Install epel repo:** </br>

```
wget https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
rpm -ivh epel-release-latest-7.noarch.rpm
```

**Cool plugins:** </br>

- presto (yum-presto) - adds support to Yum for downloading delta RPM packages
- yum-downloadonly (yum-plugin-downloadonly) - provides the --downloadonly command-line option which can be used to download packages
- Usage:
- yum install --downloadonly --downloaddir=target_dir package_name - download package_name to target_dir

**mandb** </br>

use allways: mandb commnad after installation - this updates man databases
