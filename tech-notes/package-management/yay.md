**Search for package** </br>
yay package_name

**Install package** </br>
yay -S package_name

**Update both Pacman and AUR packages** </br>
yay -Syu
