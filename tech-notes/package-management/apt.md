# [apt](https://manpages.ubuntu.com/manpages/xenial/man8/apt.8.html)

**Update the packages database** </br>
apt update

**Upgrade installed packages** </br>
apt upgrade

**Upgrade specific PACKAGE** </br>
apt upgrade PACKAGE

**Upgrade but will remove currently installed packages if this is needed to upgrade the system as a whole** </br>
apt full-upgrade

**Complete distribution upgrade** </br>
apt dist-upgrade

**Search for a package** </br>
apt-cache search SEARCH_TERM

**Information about package** </br>
apt-cache showpkg PACKAGE

**List installed packages** </br>
apt list -–installed

**Remove not needed packages installed as dependency for other packages** </br>
apt autoremove
