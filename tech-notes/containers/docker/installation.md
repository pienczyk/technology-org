# Installing Docker engine

[Docker documentation](https://docs.docker.com/engine/install/) <br>

## Ubuntu
**Install prerequisite packages** <br>
`sudo apt install apt-transport-https ca-certificates curl software-properties-common`

**Add the GPG key for the official Docker repository to your system** <br>
`curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg`

**Add the Docker repository to APT sources** <br>
`echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null`

**Update your existing list of packages again for the addition to be recognized** <br>
`sudo apt update`

**Make sure you are about to install from the Docker repo instead of the default Ubuntu repo:** <br>
`apt-cache policy docker-ce`

**Install Docker** <br>
`sudo apt install docker-ce -y`

**Check if Docker service is running** <br>
`sudo systemctl status docker`

**Enable the Docker service, if it should run on start** <br>
`sudo systemctl enabled docker`

**Add non root user to the docker group to have access to the engine** <br>
`sudo usermod -aG docker USER_NAME`
