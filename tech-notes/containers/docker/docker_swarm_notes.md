# Docker swarm
[Docker Swarm documentation](https://docs.docker.com/engine/swarm/)<br>

**Initialize docker swarm mode** <br>
`docker swarm init`

On multi IP systems specify IP to be advertised using --advertise-addr <br>

**Add node to swarm** <br>
`docker swarm join --token SWARM_TOKEN` <br>

**Get join token for worker/manager** <br>
```
docker swarm join-token worker
docker swarm join-token manager
```

**List swarm nodes** <br>
`docker node ls`

**Leave swarm cluster** <br>
`docker swarm leave`

**Remove node from the cluster (run on manager)** <br>
`docker node rm NODE_ID/HOSTNAME`

**Promote worker to manager (run on manager)** <br>
`docker node promote NODE_ID/HOSTNAME`

**Modify node's role** <br>
`docker node update --role [manager|worker] NODE_NAME`

**Change node's availibilty** <br>
Drain - no task can be assigned to a NODE_NAME <br>
`docker node update --availability drain NODE_NAME`
Active <br>
`docker node update --availability avtive NODE_NAME`

**Inspect node** <br>
`docker node inspect [--pretty] NODE_NAME`

**Create service** <br>
`docker service create IMAGE`

**Update service** <br>
Update running image <br>
`docker service update --image NEW_IMAGE_NAME SERVICE_NAME`

**Add new variable**
`docker service update --env-add NEW_VARIABLE SERVICE_NAME`

**Change/add publish port** <br>
`docker service update --publish-rm OLD_PORT --publish-add NEW_HOST_PORT:APP_PORT SERVICE_NAME`

**Add service's replicas** <br>
`docker service update --replica NUMBER_OF_REPLICAS`
or scale <br>
`docker service scale SERVICE_NAME=NUMBER_OF_REPLICAS`

**Remove service** <br>
`docker service rm SERVICE_NAME`

## Secrets - available only in swarm mode
**Create secret** <br>
`docker secret create SECRET_FILE`

**List exsisting secrets** <br>
`docker secret ls`

# Docker stack
**Create stack from compose file** <br>
`docker stack deploy -c COMPOSE_FILE STACK`
