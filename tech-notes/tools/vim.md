## Vim documentation - https://www.vim.org/docs.php

## Quitting

**In normal mode:** <br>
**:q**- if no changes to a file were made <br>
**:q!** or **ZQ** - if changes were made but you want to discard them <br>
**:wq!** or **ZZ** - if changes were made and you want to save them and quit <br>

## Movement

**h** - left <br>
**j** - down <br>
**k** - up <br>
**l** - right <br>
**ctrl+o** - jump to older position <br>
**ctrl+i** - jump to newer position <br>
**ctrl+w h|j|k|l** - jump between open windows <br>

**Jump to [line_number]** <br>
**:[line_number] + Enter** - go to [line_number] <br>
**[line_number]G** <br>
**[line_number]gg** <br>

**%** - jump between opening and closing (){}[] <br>
**z+Enter** - center the screen <br>
**zt** - bring current line to the top of a screen <br>
**z.** or **zb** - bring current line to the bottom of a screen <br>
**zz** - go to a line with cursor <br>
**gf** - if cursor is on a valid file path/name jump to the file <br>
**w** - jump word forward <br>
**W**- jump word forward, including punctuation <br>
**B** - jump word backward, including punctuation <br>
**e**- end of a next word <br>
**E** - end of a next word, including punctuation <br>
**ctrl+d** - page down <br>
**ctrl+b** - page up <br>
**:set cuc** - highlight a column with a cursor (:set nocuc - to disable it) <br>

**^** - jump to first non blank character in the line <br>
**0** - jump to beginning of the line <br>
**\$** - end of a line <br>

## Using help

**:help (or :h)** - browse help <br>
**:h _[what_to_search]_** - help for [what_to_search] <br>
**:h _[mode][what_to_search]_** - help for [what_to_search] in [mode]. Modes: i*,c*,v\_ <br>
**ctrl+]** - jumps to section under cursor <br>
**ctrl+o** - jump back (repeat to go further back).

**Options:** <br>
**:set _[option][?/!]_**- set [option]. ? - checks current setting. ! - unset [option] <br>

**For easer search:** <br>
^ = ctrl <br>
\<CR> = Enter <br>

## Text objects

**[action]i[object]** or **[action]a[object]** - access object. Objects: "", <>, {}, (), {}, html tags, paragraphs <br>
**Example:** <br>
**ci"** - change text inside "" <br>
**ca"** - change text including "" <br>
**cit** - change text in the html tag under the cursor <br>

## Editing

**i** - insert mode <br>
**I** - insert text before first non blank character in line <br>
**a** - append character after the cursor position <br>
**A** - append at the end of a line <br>
**o** - new line below <br>
**O** - new line above <br>
**s** - substitute - delete the character your cursor and enter into Insert mode <br>
**5o#+Esc** - create five new lines with # at the beginning <br>
**r** - replace one character and back to Normal mode <br>
**R** - replace mode <br>
**cw** - replace one word <br>
**ce** - change to an end of a word <br>
**cb** - change to a beginning of a word <br>
**cc** - replace entire line <br>
**C** - replace all to the end o a line <br>
**J** - join line with the cursor and a next line. Additional space will be added between lines <br>
**gJ** - join lines without space between <br>
**yy** - copy line <br>
**yl** - copy character. Add number before l to copy a number of characters: y3l - copy 3 characters <br>
**p** - paste below <br>
**P** - paste above <br>
**xp** - switch character under a cursor with next one <br>
**Xp** - switch character under a cursor with previous one <br>

**Insert special characters (in edit mode)** <br>
**ctrl+k +**: <br>

- **oo** • bullet <br>
- **Db** ◆ diamond bullet <br>
- **Pd** £ pound <br>
- **Eu** € euro <br>
- **-N** – en dash <br>
- **-M** — em <br>

## Deleting stuff in normal mode

**dw** – delete from cursor position to a beginning of a next word <br>
**dW** - delete - including non alphabetic characters - from cursor position to a beginning of a next word <br>
**daw** - delete a word under a cursor <br>
**diw** - delete inter word (leaves space before next word) <br>
**dis** - delete sentence under a cursor, including space before next sentence. <br>
**das** - delete sentence under a cursor leaving space before next sentence <br>
**dip** - delete paragraph including blank line before next paragraph <br>
**dap** - delete paragraph leaving blank line before next paragraph <br>
**d _NUMBER_ w** – delete a NUMBER of words <br>
**de** – delete to the end of the current word <br>
**d$** – delete to an end of a line <br>
**D** - as above <br>
**dd** – delete the whole line <br>
**_NUMBER_ dd** – delete number of lines <br>
**dt[character]** - delete until the character
**d/_[pattern to find]_** - delete all until [pattern] <br>
**g/_[pattern]_/d** - delete all lines with [pattern]. Omit /d to see found lines. <br>

## Undo

**ctrl+r** - re-undo last command/change <br>
**u** - undo last command/change <br>
**g+** - older text state <br>
**g-** - newer text state <br>
**:change** - show changes <br>
**:undolist** - display list of changes <br>
**:undo _[number]_** - undo to the [number] change <br>

## Copying lines:

**Entire line** <br>
Go to a beginning of the line you want to copy and press **yy**. <br>
Go to the line where you want to paste the copied line and press **p**. <br>

**Selected text** <br>
Go to the beginning of the text you want to copy and press **v**. Use arrows to mark text to copy and press **y**. <br>
Go to the line where you want to paste the copied line and press **p**. <br>

## Registers

**:reg** - see registers <br>
**"[register][action]** - use a [register] to perform [action] <br>

**Example** <br>
**"ayy** - copy a line under the cursor to registry a <br>
**"ap** - paste a line from the registry a <br>
**"Ayy** - append to the registry a <br>

## Searching

**\* and #**- search for a word under a cursor forward and backward <br>
**f[char]** - find and jump (forward) to next occurrence of the [char] <br>
**F[char]** - find and jump (back) to next occurrence of the [char] <br>
**;** - repeat search in the same direction <br>
**,** - repeat search in opposite direction <br>
**t[char]** - find and jump (forward) before next occurrence of the [char] - till <br>
**T[char]** - find and jump (back) before next occurrence of the [char] - till <br>
**/[pattern}** - search forward for first occurrence of the pattern <br>
**?[pattern}** - search backward for first occurrence of the pattern <br>
**/\c[pattern]** - case insensitive search <br>
**/\C[pattern]** - case sensitive search (default) <br>
**\*** - search for a word under a cursor <br>
**n** - repeat search in the same direction <br>
**N** - repeat search in the opposite direction <br>
**:[pattern]/{to_search}** - search {to_search} in lines between [pattern] <br>

**Examples:** <br>
**:/Global/,/Local/s/IP ADDRESS** - finds all occurrence of the "IP ADDRESS" in lines between Global and Local <br>
**:/Global/,$s/IP ADDRESS** - finds all occurrence <br>

To escape **/** in search any non-alphabetic character can be used. <br>

**Example:** <br>
**%s#/usr/bin/** - finds all occurrence of /usr/bin in the file <br>
**%s+/usr/bin** <br>

## Replacing text

**:s/old/new/g** - to replace all occurrences of old in a line
**:[range]s/old/new/g** - to replace all occurrences of old in [range]. Example: :1,5s/old/new/g - replaces all occurrence of old to new in lines 1 to 5 <br>
**:%s/old/new/g** - to replace all occurrences of old in the entire file. Same to :1,$/old/new/g <br>
**:%s/old/new/gc** - to replace all occurrences of old in the entire file, but prompt for each occurrence <br>
**:[pattern]s/old/new/g** - replace all occurrences of old to new in search pattern <br>

**Print and review all possible replacements** <br>
**:g/MATCH/#|s/MATCH/REPLACE/g|#** <br>

## Sorting

**:[range]sort[!]** - sort [range] of lines. ! Reverse order. [range] can be taken from Visual Mode selection. <br>

## Switching cases

Toggle case "HellO" to "hELLo" with **g~** then a movement. <br>
Uppercase "HellO" to "HELLO" with **gU** then a movement. <br>
Lowercase "HellO" to "hello" with **gu** then a movement. <br>

**guu** - lowercase line <br>
**gUU** - uppercase line <br>
**~** - invert case (upper->lower; lower->upper) of current character <br>

## Buffers

**:ls** or **:buffers** - list of open buffers <br>
**:b tab** - go through list of open buffers <br>
**ctrl+Shift+^** - go back and forth between current and previous buffer <br>
**:b _[unique identifier]_** - jump to file <br>
**:bd _[unique identifier]_** - close and remove current buffer <br>
_Add ! to force changing buffer if current buffer was changed but not saved <br>
**:qa** - close all open buffers and quit vim <br>
**:qw** - write all open buffers and quit vim <br>
\*\*:bufod _[command]**\* - execute [command] in each buffer in the buffer list <br>
**:bufdo %/s/OLD/NEW/g | w** - change OLD to NEW in all open buffers and save changes <br>
**:wa\*\* - save all open buffers <br>

## Multiple files

**Starting vim** <br>
vim FIILE1 ... FILEn - open multiple files in tabs <br>
vim -o FILE1 FILE2 - open multiple files in horizontal split <br>
vim -O FILE1 FILE2 - open multiple files in vertical split <br>

**Inside vim** <br>
**e: FILE_NAME** - open FILE_NAME to edit <br>
**v: FILE_NAME** - open FILE_NAME in read only mode <br>
**:ls** - list open files <br>
**:n** or **:bn** - move to next open file <br>
**:N** or **:bp** - move to previous open file <br>
**:bf** - move to first open file <br>
**:bl** - move to last open file <br>
**:wall** or **:wa** - save all opened files <br>
**:qall** or **:qa** - close all opened files <br>

## Windows

**:split** or **:sp** - horizontal split <br>
**:vsplit** or **:vsp** - vertical split <br>
**ctrl+w w** - circle through open windows <br>
**ctrl+w + movement** - move between split windows <br>
**ctrl+w [number]+** - to resize the height of the current window by [number] rows <br>
**ctrl+w +** and **ctrl+w -**:resize - resize hight <br>
**ctrl+w >** and **ctrl+w <** :vertical resize - resize width <br>
**ctrl+w \_** - maximise hight <br>
**ctrl+w |** - maximise width <br>
**ctrl+=** - make all open windows the same size <br>
**ctrl+w r** and **ctrl+w R** - rotate open windows <br>
**ctrl+w H/J/K/L** - rotate active window in chosen direction <br>
**:only** - close all other windows <br>
**:windo _[command]_** - run command in all open windows <br>

## Tabs

**Open files in multiple tabs** <br>
`vim -p file1 file2 file3` <br>

**Inside vim** <br>
**:tabedit {file}** - edit specified file in a new tab <br>
**:tabfind {file}** - open a new tab with filename given, searching the 'path' to find it <br>
**:tabclose** - close current tab <br>
**:tabclose {i}** - close i-th tab <br>
**:tabonly** - close all other tabs (show only the current tab) <br>
**:tabs** - list of opened files <br>
**:tabn** - go to next tab - in normal mode gt <br>
**:tabp** - go to previous tab - in normal mode gT <br>
**:tabfirst** - go to first tab <br>
**:tablast** - go to last tab <br>

**Save current layout to a file** <br>
**:mksession _[file-name]_** - save current layout to a [file-name] <br>
**vim -S _[file-name]_** - load saved layout <br>

## Macros

**q[letter]** - start recording a macro @[letter] <br>
**q** - stop recording a macro <br>
**@[number][letter]** - apply recorded macro [number] of times <br>
**q[LETTER]** - append to created macro under @[letter] <br>
**:[range] normal @[macro]** - apply recorded macro to [range] lines <br>
**:%normal @[macro]** - apply recorded macro from the position of the cursor to the end of a file <br>
**:registry/:reg** - show recorded macros <br>

**Examples** <br>
**2,20 norm \@w** - apply macro "w" to lines 2-20 <br>
**2,$ norm \@w** - apply macro "w" from line 2 to an end of a file <br>

## Highlighted search

**:set hlsearch** - highlight search <br>
**:nohl** - disable highlight <br>
**:noh**- Clean selection <br>

## Numbered lines

**:set incsearch** - incremental search <br>
**:set autoindet** - auto tabulate <br>

## Spelling check

**:set spell spelling=en_us** <br>
**:set nospell** <br>
**]s** and **[s** - moves cursor between misspelled words <br>
**z=** - shows suggested corrections <br>

## Autocompletion

**ctrl+n** - from opened file forward <br>
**ctrl+p** - from opened file backward <br>
**ctrl+e** - exit from autocompletion list and go back to typing <br>
**ctrl+x** **ctrl+f** - autocomplete directories/files names. ctrl+f - go through files list <br>

## Comment out/in multiple lines

**To comment out** <br>
Press **ctrl+v** to enter Visual Block mode. <br>
Select lines to comment out. <br>
Press **I** and insert **#**. <br>
Press **ESC**. Save the file. <br>

**To comment in** <br>
Press **ctrl+v** to enter Visual Block mode. <br>
Select lines to comment in. <br>
Press **x** <br>

**In visual mode** <br>
Press **v** to mark lines to be commented in/out <br>
**comment in** <br>
:norm i# <br>
**comment out** <br>
:norm x <br>

## Indent blocks of text

Press **ctrl+v** to enter Visual Block mode. <br>
Select lines to be indented and press **>** <br>

**More on indenting** <br>
**\>>** - Indent line by shiftwidth spaces <br>
**\<<** - De-indent line by shiftwidth spaces <br>
**5>>** - Indent 5 lines <br>
**5==** - Re-indent 5 lines <br>

**>%** - Increase indent of a braced or bracketed block (place cursor on brace first) <br>
**=%** - Re-indent a braced or bracketed block (cursor on brace) <br>
**\<%** - Decrease indent of a braced or bracketed block (cursor on brace) <br>
**]p** - Paste text, aligning indentation with surroundings <br>

**=i{** - Re-indent the 'inner block', i.e. the contents of the block <br>
**=a{** - Re-indent 'a block', i.e. block and containing braces <br>
**=2a{** - Re-indent '2 blocks', i.e. this block and containing block <br>

**\>i{** - Increase inner block indent <br>
**<i\{** - Decrease inner block indent <br>

**Re-indent a file** <br>
In normal mode press **gg=G** <br>

## Running shell commands

**:shell** - get shell in vim. Type exit to go back to the vim. <br>
**:!_[command]_** - run command and get results in an open buffer <br>

**Get shell command result in the edited file** <br>
**:r !{cmd}** <br>

## Visual Mode

**v** - characterwise visual mode <br>
**V** - linewise visual mode <br>
**ctrl+v** - blockwise visual mode <br>
**gv** - reselect a previous selection <br>
**:** - run command on selected block (works linewise) <br>
**:['<,'>][center, left, right][number of columns]** - justify selected text. If [number of columns] not used default 80 columns will be used. <br>

**Inside visual mode** <br>
**o** - jump between a beginning and an end of current selection <br>

## Vimdiff

**:diffoff!** - disable coloring <br>
**:windo diffthis** - enabling coloring <br>

For instruction on how to use the Vim as a merge tool for Git, go to [Using vimdiff as merge tool](https://gitlab.com/pienczyk/pienczyk-priv/-/blob/master/tech-notes/tools/git.md#using-vimdiff-as-merge-tool) <br>

## Misc

**:g/^$/d** - remove empty lines <br>
**:g/WORD/d** - remove lines containing <br>
**vim FILE_NAME +/PATTERN** - open FILE_NAME on the PATTERN <br>

**Most available in the normal mode.** <br>
(vimrc) Can be added to .vimrc to be persistent otherwise valid for current session or until disabled. Most command can be disabled by adding "no". _Example_: :set number (enables line numbering), :set nonumber (disables it) <br>

**:e** FILE_PATH - open FILE <br>
**q:** - open a list of latest commands <br>
**:version** - VIM's configuration info <br>
\# or \* - find all instances of a word under cursor <br>
**ctrl+g** - get number of lines in the file <br>
**:set nocompatible** - disable vi compatibility (vimrc) <br>
**:set path+=** - (vimrc) enable file searching in all subdirectories under working one. Also allows for files name autocompletion (with tab). It also allows for wild cards. <br>
**_Example:_** :find \*.txt - search for all files with txt extensions. <br>
**:set paste** - (vimrc) - enable formatted pasting <br>
**:read [file]** - inserts [file] content to current file (buffer) <br>
**:set ff=[format]** - sets <EOL> (end of lines) to [format]. Formats: dos, Unix,mac <br>
**:set [setting]?** - check state of the [setting] <br>
**_Example:_**:set path? - displays path where vim searches for files (vimrc) <br>
**:find [file_name]** - search for a files. By default in the working directory. If set path+=vimrc configured in the .vimrc also in all subdirectories under. <br>
**:noh** - Clean selection <br>
**:set incsearch** - incremental selection (vimrc) <br>
**:set hlsearch** - enable search highlighting (vimrc) <br>
**:echo \@%** - directory/name of edited file <br>
**:file** - display name of edited file <br>
**:set showcmd** - show typed command <br>
**:put=strftime('%c')** - insert cursor time in normal mode <br>
**ctr+r=put=strftime('%c')** - insert current time in normal model <br>
**vaw** - Visually select Around Word <br>
**dap** - Delete Around Paragraph <br>
**:set scrollbind** / **:set scb** - set in each splitted windows to scroll synchronously <br>
**g ctrl+g** - detailed information about the current file, like number of words or how many bytes <br>
**gx** - Open a link under cursor in a browser <br>

### Increase/decrease numbers (out cursor on the number):

**ctrl+a** - increase <br>
**crrl+x** - decrease <br>

### Search for not needed white spaces

**/\t** - Show all tabs <br>
/\s\+$ - Show trailing white space <br>
**/\S\zs\s\+$** - Show trailing white space only after some text (ignores blank lines) <br>
**/ \+\ze\t** - Show spaces before a tab <br>

### Counting selected lines <br>

In Visual Mode: **g Ctrl+g**

### Edit files remotly over ssh (Vim version > 6 )

`vim scp://foo.example.com//path/to/file` <br>

**When in Vim allready** <br>
**:e scp://foo.example.com//path/to/file** <br>

### Sudo inside "read only session"

**:w !sudo tee %** <br>

### Plugins

**vim-plug** - A minimalist Vim plugin manager <br>

**vim-surround** - quickly surround words with ' " {} () []. Also allows to replace surrounding characters. <br>

**ys[number]w[surrounding char]** - surround [number] of words with [surrounding char] <br>

**Example: ys3w" - surround 3 words from the cursor with "** <br>
**ysw]** - surround a word with [] without spaces = [word] <br>
**ysw[** - surround a word with [] with spaces = [ word ] <br>
**cs'"**- change '' to "" <br>
**ds[surrounding]** - delete [surrounding] <br>
**ds\*** - deletes the innermost \* surroundings (if any) of the text under the cursor <br>

**vim-fugitive** - Git plugin <br>
**:Git [Tab]** - to see all commands <br>

**Examples:** <br>
**:Git diff** - show diff on currently edited file <br>
**:Git commit -m ""** - commit <br>
**:Git push** - push <br>

**vim-flog** - a lightweight and powerful git branch viewer that integrates with fugitive <br>

**vim-airline** - status line <br>
**vim-airline-themes** - official repository with themes for the vim-airline <br>

**aserebryakov/vim-todo-lists** - TODO lists management <br>

### Ideal for a Friday

**:smile** <br>
**:h 42** <br>
:**Ni!** <br>
