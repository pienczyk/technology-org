## GREP

[Documentation](https://www.gnu.org/software/grep/manual/grep.html)

**String in quotes** <br>
`grep -oP '"\K[^"\047]+(?=["\047])'`

**Search for text in files (including binary files)** <br>
`grep -arnw PATH -e [PATTERN]`

**Invert search** <br>
`grep -v [PATTERN]`

**Recursive search** <br>
`grep -r [PATTERN]`

**Grep number of lines before match** <br>
`grep -B NUMBER_OF_LINES [PATTERN]`

**Grep number of lines after match** <br>
`grep -A NUMBER_OF_LINES [PATTERN]`

**Grep number of lines before and after** <br>
`grep -C NUMBER_OF_LINES [PATTERN]`

**Ignore case sensitive** <br>
`grep -i [PATTERN]`

**Show line numbers** <br>
`grep -n [PATTERN]`

**Search for multiple words** <br>
`grep -E "(backup|root|syslog)" /etc/passwd`

**List files' names where PATTERN was found** <br>
`grep -l [PATTERN] [FILE]`

**Print only exact match** <br>
`grep -x [PATTERN] [FILE]`

**Use PATTERN file** <br>
`grep -f [PATTERN_FILE] [FILE_TO_MATCH]`

**Use mulitple PATTERNs** <br>
`grep -e [PATTERN1] -e [PATTERN2] -e [PATTERN3]...[FILE]`
