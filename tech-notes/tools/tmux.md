## Tmux

Out of box tmux uses ctrl+b so all below command will work with ctrl+b. To change it to ctrl+a add following lines to `~/.tmux.conf`: <br>
```
##replace ctrl+b with ctrl+a
unbind-key -n C-a
set -g prefix ^A
set -g prefix2 F12
bind a send-prefix
```

**ctrl+a w** - list of windows <br>
**ctrl+a z** - zoom/unzoom pane <br>

**Print screen** <br>
Copy entire visible contents of pane to a buffer <br>
`ctrl+a : capture-pane`

**Show all buffers** <br>
`ctrl+a : list-buffers`

**Save buffer contents to OUTPUT_FILE** <br>
`ctrl+a : save-buffer OUTPUT_FILE`

**Show all buffers and paste selected** <br>
`ctrl+a : choose-buffer`

**Display buffer_0 contents** <br>
`ctrl+a : show-buffer`

**Delete buffer_#** <br>
`ctrl+a : delete-buffer -b #`

**Reload tmux profile** <br>
`ctrl+a : source-file ~/.tmux.conf`
