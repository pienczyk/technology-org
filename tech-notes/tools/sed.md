## Sed
[Documentation](https://www.gnu.org/software/sed/manual/sed.html)

**Replace first match of the PATTERN** <br>
`sed 's/PATTERN/NEW_PATTERN/'`

**Replace all matches of the PATTERN** <br>
`sed 's/PATTERN/NEW_PATTERN/g'`

**Replace the last occurrence only of a match on each line** <br>
`sed 's/\(.*\)PATTERN/\1NEW_PATTERN/'`

**Replace the first match** <br>
`sed '1 s/PATTERN/NEW_PATTERN/'`

**Replace the last match** <br>
`sed -e '$s/PATTERN/NEW_PATTERN/'`

**Remove lines starting with** <br>
`sed '/^#/d'`

**Replace everything after PATTERN** <br>
`sed [-i] "s/PATTERN $DUMP.*/NEW_PATTERN/"`


### [Nice list of useful examples for sed usage](https://linuxhint.com/50_sed_command_examples/)
