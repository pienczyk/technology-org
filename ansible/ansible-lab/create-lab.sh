#!/usr/bin/env bash
# Script:
#   - asks for number of hosts running each OS;
#   - creates inventory file;
#   - creates docker-compose.yml file;
#   - runs docker-compose to build the lab.

# Variables:
TMPINVENTORY="./templates/inventory-tmp"
INVENTORY="./shared-vol/inventory"
TMPCOMPOSE="./templates/docker-compose-base.yml"
COMPOSE="${PWD}/docker-compose.yml"
TMPUBU="./templates/ubuntu-tmp.yml"
TMPROCKY="./templates/rocky-tmp.yml"
SUBNET='172.19.0.'
STARTIP='2'

# Functions
# Create and add entries to the docker-compose.yml
create_compose_entries()
{
    if [[ ${1} == 'Ubuntu' ]]
    then
        HOSTPREFIX='ubu-host'
        COMPTEMP=${TMPUBU}
        INVTEMP='temp_ubu'
    else
        HOSTPREFIX='rocky-host'
        COMPTEMP=${TMPROCKY}
        INVTEMP='temp_rocky'
    fi

    #Add entries for Ubuntu host to the docker-compose.yml
    for i in $(eval echo "{1..$2}")
        do
            cat ${COMPTEMP} >> ${COMPOSE}
            TEMPHOSTNAME="${HOSTPREFIX}${i}"
            sed -i -e 's/temp_host/'"${TEMPHOSTNAME}"'/g' ${COMPOSE}
            STARTIP=$((STARTIP+1))
            TEMPIP=${SUBNET}${STARTIP}
            sed -i -e 's/IP_ADDRESS/'"${TEMPIP}"'/g' ${COMPOSE}
    done
    TEMPHOSTNAME="${HOSTPREFIX}[1:${2}]"
    sed -i -e 's/'"${INVTEMP}"'/'"${TEMPHOSTNAME}"'/g' ${INVENTORY}
}

# Check if Docker and Docker Compose are installed
[[ -f $(which docker) ]] && docker_installed="true" || docker_installed="false"
docker compose &>/dev/null
[[ $? -eq 0 ]] && compose_installed="true" || compose_installed="false"
[[ "$docker_installed" == "false" && $compose_installed == "false" ]] && printf "Docker and/or Docker compose is not installed." && exit 1

# Check if user has rights to write and execute files in the current folder
#[[ ! -x ${PWD} ]] || [[ ! -w ${PWD} ]] && echo "${USER} cannot write/execute in the current directory. Fix permissions or clone the repository to different location." && exit 1

# Prompt for number of managed hosts
echo "Please specify how many managed host you need:"
read -p "Ubuntu based: " UBU_HOSTS
read -p "Rocky based: " ROCKY_HOSTS

if [[ ${UBU_HOSTS} -eq '0' ]] && [[ ${ROCKY_HOSTS} -eq '0' ]]
then
    echo "You choose to not create any managed hosts"
    exit 0
else
    mkdir -p ${PWD}/shared-vol/
    # Copy base docker-compose.yml and base inventory files to work directory
    cp ${TMPCOMPOSE} ${COMPOSE}
    cp ${TMPINVENTORY} ${INVENTORY}

    # Create selected number of Ubuntu based hosts
    if [[ ${UBU_HOSTS} -ne '0' ]]
    then
        create_compose_entries "Ubuntu" ${UBU_HOSTS}
    else
        echo "Zero Ubuntu hosts created."
    fi

    # Create selected number of Rocky based hosts
    if [[ ${ROCKY_HOSTS} -ne '0' ]]
    then
        create_compose_entries "Rocky" ${ROCKY_HOSTS}
    else
        echo "Zero Rocky hosts created."
    fi
fi

# Build and bring up created lab
[[ -f ${COMPOSE} ]] && [[ -f ${INVENTORY} ]] && docker compose up -d --build && exit 0
