Base Host
=========

Prepere base instance of the Ubuntu or CentOS server; set hostname, install updates and tools from vars.

Requirements
------------

Own inventory files with IP addresses and hostnames. Files to be copied to the machine with the copy-files.yml task needs to be added to files/ and list must be modified in the vars/main.yml.

Role Variables
--------------

vars/main.yml

- packages - list of tools to be installed
- files - list of files to be copied to the target machine
- tags: base, reboot

Dependencies
------------

None

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - base-host

License
-------

MIT

Author Information
------------------

pienczyk
