Docker Host
=========

Role prepers Ubuntu/CentOS system for the Docker host. It will install all required tools + Docker Compose.
Adjust user/password in vars.

Requirements
------------

Ubuntu/CentOS system with configured access from the Ansible managment host.

Role Variables
--------------

- packages - Docker prerequisites for the Ubuntu system
- user_name, user_password - for docker user creation
- tags: docker, compose, user

Dependencies
------------

None.

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - docker-host

License
-------

MIT

Author Information
------------------

pienczyk
