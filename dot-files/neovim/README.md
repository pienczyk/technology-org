# [Neovim](https://neovim.io/)

Task: Make the Neovim a main IDE.

### Config directories structure

The Neovim follows the XDG directory structure. The configuration directory goes to the $XDG_CONFIG_HOME.

```
.config/nvim
├── init.lua
├── lazy-lock.json
└── lua
    └── rafal
        ├── core
        │   ├── init.lua
        │   ├── keymaps.lua
        │   └── options.lua
        ├── lazy.lua
        └── plugins
            ├── autopairs.lua
            ├── bbye.lua
            ├── bufferline.lua
            ├── cmp.lua
            ├── colorscheme.lua
            ├── conform.lua
            ├── dressing.lua
            ├── gitsigns.lua
            ├── lazygit.lua
            ├── lsp
            │   ├── lspconfig.lua
            │   └── mason.lua
            ├── lualine.lua
            ├── nvim-tree.lua
            ├── surround.lua
            ├── telescope.lua
            ├── todo-comments.lua
            ├── trouble.lua
            ├── vim-lessmess.lua
            ├── vim-maximizer.lua
            ├── vim-remote-sshfs.lua
            └── vim-todo-lists.lua
```

### Dependencies

- git
- LazyGit
- ripgrep
- fzf

### Installed plugins

- **Autopairs** - A super powerful autopair plugin for Neovim that supports multiple characters.
- **Vim-Bbye** - Bbye allows you to do delete buffers (close files) without closing your windows or messing up your layout.
- **Bufferline** - A snazzy buffer line (with tabpage integration) for Neovim built using lua.
- **Nvim-cmp** - A completion engine plugin for neovim written in Lua. Completion sources are installed from external repositories and "sourced".
- **Comments** - Smart and Powerful commenting plugin for neovim.
- **Gitsigns** - Super fast git decorations implemented purely in Lua.
- **LazyGit** - Plugin for calling lazygit from within neovim.
- **Lualine** - A blazing fast and easy to configure Neovim status line written in Lua.
- **NvimTree** - A File Explorer For Neovim Written In Lua.
- **Surround** - Surround selections, stylishly.
- **Telescope** - telescope.nvim is a highly extendable fuzzy finder over lists. Built on the latest awesome features from neovim core.
- **Todo-Comments** - todo-comments is a lua plugin for Neovim >= 0.8.0 to highlight and search for todo comments like TODO, HACK, BUG in your code base.
- **Trouble** - A pretty list for showing diagnostics, references, telescope resultsA pretty list for showing diagnostics, references, telescope results.
- **Vim-maximizer** - Maximizes and restores the current window in Vim.
- **Vim-remote-sshfs** - Explore, edit, and develop on a remote machine via SSHFS with Neovim.
- **Vim-todo-lists** - vim-todo-lists is a Vim plugin for TODO lists management.
- **Conform** - Lightweight yet powerful formatter plugin for Neovim
- **Mason** - Portable package manager for Neovim that runs everywhere Neovim runs.
- **Nvim-lspconfig** - Configs for the Nvim LSP client.
- **Dressing** - cmp/lsp helper
- **Vim-lessmess** - clear unnecessary whitespaces

### Configured shortcuts

Leader key is set to the Space bar </br>

**General** </br>

- `<leader>hs` - horizontal split
- `<leader>vs` - vertical split
- `<leader>h / <C-w>h` - move to window on the left
- `<leader>j / <C-w>j` - move to window on the down
- `<leader>k / <C-w>k` - move to window on the up
- `<leader>l / <C-w>l` - move to window on the right
- `<C-Up>` - resize up by 2
- `<C-Down>` - resize down by 2
- `<C-Left>` - resize left by 2
- `<C-Right>` - resize right by 2
- `<leader>sm` - maximize current window
- `<leader>fl` - show all open files
- `<leader>f` - file finder (Telescope)
- `<leader>w` - write
- `<leader>q` - close opened buffer (tab)
- `<leader>n` - clear selections
- `<leader>c` - show cursor column
- `<leader>nc` - hide cursor column
- `<leader>es` - enable spelling check (english)
- `<leader>ds` - disable spelling check
- `<leader>vt` - vertical split with a terminal
- `<leader>ht` - horizontal split with a terminal
- `<C-[>` - move to prev open file (tab)
- `<C-]>` - move to next open file (tab)
- `;;` - ESC

**Plugins' specific** </br>

**cmp** </br>

- `<C-k>` - previous suggestion
- `<C-j>` - next suggestion
- `<C-b>` - scroll docs
- `<C-f>` - scroll docs
- `<C-e>` - close completion window
- `<CR>` - select suggestion
- `<C-Space>` - show completion suggestions
- `<Tab>` - scroll trought suggestions

**Telescope** </br>

- `<leader>f` - Telescope find files
- `<leader>of` - Telescope find old files
- `<leader>lg` - Telescope live grep
- `<leader>gc` - Telescope grep_string
- `<leader>td` - ToDoTelescope
- `gr` - lsp reference

**Nvimtree** </br>

- `<leader>e` - Open NvimTree with size 30
- `<leader>ef` - Open NvimTree in the directory of the current file
- `<leader>ec` - Colapse NvimTree
- `<leader>er` - Refresh NvimTree

`Tab` - open a selected file and keep the cursor in the Nvimtree
`R` - (refresh) to perform a reread of the files contained in the project
`H` - (hide) to hide/display hidden files and folders (beginning with a dot .)
`E` - (expand_all) to expand the entire file tree starting from the root folder (workspace)
`W` - (collapse_all) to close all open folders starting from the root foldero0
`-` - (dir_up) allows you to go back up folders. This navigation also allows you to exit the root folder (workspace) to your home directory
`s` - (system) to open the file with the system application set by default for that file type
`f` - (find) to open the interactive file search to which search filters can be applied
`F` - to close the interactive search
`<C+k>` - to display information about the file such as size, creation date, etc.
`g + ?` - to open the help with all the predefined shortcuts for quick reference
`q` - to close the file explorer
`a` - (add) allows the creation of files or folders, creating a folder is done by following the name with the slash /. E.g. /nvchad/nvimtree.md will create the related markdown file while /nvchad/nvimtree/ will create the nvimtree folder. The creation will occur by default at the location where the cursor is in the file explorer at that time, so the selection of the folder where to create the file will have to be done previously or alternatively you can write the full path in the statusline, in writing the path you can make use of the auto-complete function
`r` - (rename) to rename the selected file from the original name
`<C+r>` - to rename the file regardless of its original name
`d` - (delete) to delete the selected file or in case of a folder delete the folder with all its contents
`x` - (cut) to cut and copy the selection to the clipboard, can be files or folders with all its contents, with this command associated with the paste command you make the file moves within the tree
`c` - (copy) like the previous command this copies the file to the clipboard but keeps the original file in its location
`p` - (paste) to paste the contents of the clipboard to the current location
`y` - to copy only the file name to the clipboard, there are also two variants which are Y to copy the relative path and g + y to copy the absolute path

**LazyGit** </br>

- `<leader>git` - LazyGit current file

**Surround** </br>

```
--  Old text                    Command         New text
----------------------------------------------------------------------------------
--    surr*ound_words             ysiw)           (surround_words)
--    *make strings               ys$"            "make strings"
--    [delete ar*ound me!]        ds]             delete around me!
--    remove <b>HTML t*ags</b>    dst             remove HTML tags
--    'change quot*es'            cs'"            "change quotes"
--    <b>or tag* types</b>        csth1<CR>       <h1>or tag types</h1>
--    delete(functi*on calls)     dsf             function calls
```

**Conform** </br>

- `<leader>mp - run file formatter`

**Nvim-lspconfig** </br>

- `gr` - Telescope lsp_reference
- `K` - Show documentation for what is under cursor
- `<leader>ca` - see available code actions

**Dressing** </br>

- `<leader>rn` - bulk rename objects

**Comment** </br>

```
NORMAL mode
- gcc - Toggles the current line using linewise comment
- gbc - Toggles the current line using blockwise comment
- [count]gcc - Toggles the number of line given as a prefix-count using linewise
- [count]gbc - Toggles the number of line given as a prefix-count using blockwise
- gc[count]{motion} - (Op-pending) Toggles the region using linewise comment
- gb[count]{motion} - (Op-pending) Toggles the region using blockwise comment

VISUAL mode
- gc - Toggles the region using linewise comment
- gb - Toggles the region using blockwise comment

Extra mappings. These mappings are enabled by default. (config: mappings.extra)
- NORMAL mode
- gco - Insert comment to the next line and enters INSERT mode
- gcO - Insert comment to the previous line and enters INSERT mode
- gcA - Insert comment to end of the current line and enters INSERT mode
```
