-- Variables
-- nice colors
-- desert
-- evening
-- habamax
-- slate
-- darkplus

--`return {
--` "eddyekofo94/gruvbox-flat.nvim",
--` priority = 1000, -- load before all other plugins
--` config = function()
--`   -- load the color scheme here
--`   vim.cmd([[colorscheme gruvbox-flat]])
--`  end,
--`}

--return {
--	"maxmx03/solarized.nvim",
--	lazy = false,
--	priority = 1000,
--	config = function()
--		vim.o.background = "dark" -- or 'light'
--
--		vim.cmd.colorscheme("solarized")
--	end,
--}

return {
	"ellisonleao/gruvbox.nvim",
	priority = 1000,
	config = true,
	-- opts = ...
}
