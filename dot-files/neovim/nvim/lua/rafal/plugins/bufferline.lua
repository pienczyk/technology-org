-- A snazzy buffer line (with tabpage integration) for Neovim built using lua.
-- source: https://github.com/akinsho/bufferline.nvim
return {
	"akinsho/bufferline.nvim",
	dependencies = { "nvim-tree/nvim-web-devicons" },
	version = "*",
	opts = {
		options = {
			-- mode = "tabs",
			-- numbers = "none", -- "none" | "ordinal" | "buffer_id" | "both" | function({ ordinal, id, lower, raise })
			offsets = { { filetype = "NvimTree", text = "", padding = 1 } },
			seperate_style = "slant",
			close_command = "Bdelete! %d",
			indicator_icon = "▎",
			buffer_close_icon = "", -- buffer_close_icon = '',
			modified_icon = "●",
			close_icon = "", -- close_icon = '',
			left_trunc_marker = "",
			right_trunc_marker = "",
			show_buffer_icons = true,
			show_buffer_close_icons = true,
			show_close_icon = true,
			show_tab_indicators = true,
			persist_buffer_sort = true, -- whether or not custom sorted buffers should persist
			-- can also be a table containing 2 custom separators
			-- [focused and unfocused]. eg: { '|', '|' }
			separator_style = "thin", -- | "thick" | "thin" | { 'any', 'any' },
			enforce_regular_tabs = true,
			always_show_bufferline = true,
			-- sort_by = 'id' | 'extension' | 'relative_directory' | 'directory' | 'tabs' | function(buffer_a, buffer_b)
			--   -- add custom logic
			--   return buffer_a.modified > buffer_b.modified
			-- end
			hover = {
				enabled = true,
				delay = 200,
				reveal = { "close" },
			},
		},
	},
}
