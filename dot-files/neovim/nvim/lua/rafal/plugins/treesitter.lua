-- Treesitter configurations and abstraction layer for Neovim.
-- source: https://github.com/nvim-treesitter/nvim-treesitter
return {
	"nvim-treesitter/nvim-treesitter",
	branch = "master",
	event = { "InsertEnter" },
	dependencies = {},
	config = function()
		require("nvim-treesitter.configs").setup({
			ensure_installed = {
				"c",
				"bash",
				"lua",
				"python",
				"terraform",
				"yaml",
				"make",
				"json",
				"vim",
				"vimdoc",
				"query",
				"http",
				"helm",
				"csv",
				"dockerfile",
				"markdown",
				"markdown_inline",
			},
			auto_install = true,
			ignore_install = {},
			sync_install = true,
			modules = {},
			highlight = {
				enable = true,
			},
		})
	end,
}
