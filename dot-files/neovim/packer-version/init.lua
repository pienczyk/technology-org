require "rafal.options"
require "rafal.keymaps"
require "rafal.colorscheme"
require "rafal.plugins"
require "rafal.cmp"
require "rafal.lsp"

-- Plugins' configs
require "rafal.nvim-tree"
require "rafal.gitsigns"
require "rafal.telescope"
require "rafal.nvim-autopairs"
