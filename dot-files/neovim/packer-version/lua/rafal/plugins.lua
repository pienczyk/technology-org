-- Variables
local fn = vim.fn
local install_path = fn.stdpath "data" .. "/site/pack/packer/start/packer.nvim"

-- Automatically install packer
if fn.empty(fn.glob(install_path)) > 0 then
  PACKER_BOOTSTRAP = fn.system {
    "git",
    "clone",
    "--depth",
    "1",
    "https://github.com/wbthomason/packer.nvim",
    install_path,
  }
  print "Installing packer close and reopen Neovim..."
  vim.cmd [[packadd packer.nvim]]
end

-- Autocommand that reloads neovim whenever you save the plugins.lua file
vim.cmd [[
  augroup packer_user_config
    autocmd!
    autocmd BufWritePost plugins.lua source <afile> | PackerSync
  augroup end
]]

-- Use a protected call so we don't error out on first use
local status_ok, packer = pcall(require, "packer")
if not status_ok then
  return
end

-- Have packer use a popup window
packer.init {
  display = {
    open_fn = function()
      return require("packer.util").float { border = "rounded" }
    end,
  },
}

-- Install your plugins here
return packer.startup(function(use)

  -- My plugins here
  -- Have packer manage itself
  use "wbthomason/packer.nvim"

  -- An implementation of the Popup API from vim in Neovim
  use "nvim-lua/popup.nvim"

  -- Useful lua functions used ny lots of plugins
  use "nvim-lua/plenary.nvim"

  -- Git
  use "tpope/vim-fugitive"
  use "rbong/vim-flog"
  use "lewis6991/gitsigns.nvim"

  -- Airline - Lean & mean status/tabline
  use "vim-airline/vim-airline"
  use "vim-airline/vim-airline-themes"

  -- surround.vim
  use "tpope/vim-surround"

  -- todo list
  use "aserebryakov/vim-todo-lists"

  -- vim as man pager
  use "lambdalisue/vim-manpager"

  -- NvimTree
  use "nvim-tree/nvim-tree.lua"

  -- fzf.vim
  use "junegunn/fzf"
  use "junegunn/fzf.vim"

  -- terminal
  use "voldikss/vim-floaterm"

  -- Color schemes
  use "lunarvim/darkplus.nvim"

  -- completion
    -- cmp plugins
    use "hrsh7th/nvim-cmp" -- The completion plugin
    use "hrsh7th/cmp-buffer" -- buffer completions
    use "hrsh7th/cmp-path" -- path completions
    use "hrsh7th/cmp-cmdline" -- cmdline completions
    use "hrsh7th/cmp-nvim-lua" -- cmdline completions
    use "saadparwaiz1/cmp_luasnip" -- snippet completions-- cmp plugins

    -- snippets
    use "L3MON4D3/LuaSnip" --snippet engine
    use "rafamadriz/friendly-snippets" -- a bunch of snippets to use

    -- lsp
    use "neovim/nvim-lspconfig" -- enable LSP
    use "williamboman/mason.nvim" -- simple to use language server installer
    use "williamboman/mason-lspconfig.nvim" -- simple to use language server installer
    use 'jose-elias-alvarez/null-ls.nvim' -- LSP diagnostics and code actions

    -- icons
    use 'kyazdani42/nvim-web-devicons'

    -- nvim-sshs
    -- use "DanielWeidinger/nvim-sshfs"

    -- Telescope
    use "nvim-telescope/telescope.nvim"
    use 'nvim-telescope/telescope-media-files.nvim'

    -- autopairs
    use "windwp/nvim-autopairs"

  -- Automatically set up your configuration after cloning packer.nvim
  -- Put this at the end after all plugins
  if PACKER_BOOTSTRAP then
    require("packer").sync()
  end
end)
