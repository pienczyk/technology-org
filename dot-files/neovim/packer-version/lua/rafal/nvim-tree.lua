-- vim-tree
-- disable netrw at the very start of your init.lua
--vim.g.loaded_netrw = 1
--vim.g.loaded_netrwPlugin = 1

-- variables
-- local tree_cb = nvim_tree_config.nvim_tree_callback
-- setup
require("nvim-tree").setup({
  disable_netrw = true,
  hijack_netrw = true,
  sort = {
    sorter = "case_sensitive",
  },
  view = {
    width = 30,
    side = "left",
    --autoresize = true,
  },
  diagnostics = {
      enable = true,
      icons = {
          hint = "",
          info = "",
          warning = "",
          error = "",
      },
  },
  git = {
    enable = true,
    ignore = true,
    timeout = 500,
  },
  renderer = {
    group_empty = true,
    highlight_git = true,
    icons = {
      show = {
        file = true,
        folder = true,
        folder_arrow = true,
        git = true,
      },
      glyphs = {
        default = "",
        symlink = "",
        git = {
            unstaged = "",
            staged = "S",
            unmerged = "",
            renamed = "➜",
            deleted = "",
            untracked = "U",
            ignored = "◌",
        },
        folder = {
            default = "",
            open = "",
            empty = "",
            empty_open = "",
            symlink = "",
        },
    },
  },
},
  filters = {
    dotfiles = false,
  },
})
--
