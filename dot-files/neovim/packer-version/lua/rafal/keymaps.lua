---
-- Variables
local opts = { noremap = true, silent = true }
local term_opts = { silent = true }
local keymap = vim.api.nvim_set_keymap -- Shorten function name

-- Remap space as leader key
keymap("", "<Space>", "<Nop>", opts)
vim.g.mapleader = " "

-- Modes
--   normal_mode = "n",
--   insert_mode = "i",
--   visual_mode = "v",
--   visual_block_mode = "x",
--   term_mode = "t",
--   command_mode = "c",

-- Normal --
-- File explorer
keymap("n", "<leader>e", ":NvimTreeOpen 30<cr>", opts)
keymap("n", "<leader>ce", ":NvimTreeClose <cr>", opts)

-- Split
keymap("n", "<leader>hs", ":split<cr>", opts)
keymap("n", "<leader>vs", ":vsplit<cr>", opts)

-- Better window navigation
keymap("n", "<leader>h", "<C-w>h", opts)
keymap("n", "<leader>j", "<C-w>j", opts)
keymap("n", "<leader>k", "<C-w>k", opts)
keymap("n", "<leader>l", "<C-w>l", opts)

-- Resize with arrows
keymap("n", "<C-Up>", ":resize +2<CR>", opts)
keymap("n", "<C-Down>", ":resize -2<CR>", opts)
keymap("n", "<C-Left>", ":vertical resize -2<CR>", opts)
keymap("n", "<C-Right>", ":vertical resize +2<CR>", opts)

-- Save
keymap("n", "<leader>w", ":w<cr>", opts)

-- Quit
keymap("n", "<leader>q", ":q<cr>", opts)

-- Show open files
keymap("n", "<leader>fl", ":files<cr>", opts)

-- Clear highligt
keymap("n", "<leader>n", ":noh<cr>", opts)

-- Highlight a column with a cursor
keymap("n", "<leader>c", ":set cuc<cr>", opts)
keymap("n", "<leader>nc", ":set nocuc<cr>", opts)

-- keymap("n", "<leader>f", "<cmd>Telescope find_files<cr>", opts)
--keymap("n", "<leader>f", "<cmd>lua require'telescope.builtin'.find_files(require('telescope.themes').get_dropdown({ previewer = 30 }))<cr>", opts)
keymap("n", "<leader>f", "<cmd>lua require('telescope.builtin').find_files({ previewer = false })<cr>", opts)
keymap("n", "<leader>lg", "<cmd>Telescope live_grep<cr>", opts)
--
