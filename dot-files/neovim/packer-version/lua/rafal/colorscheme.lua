-- Variables
  -- nice colors
  -- desert
  -- evening
  -- habamax
  -- slate

local colorscheme = "darkplus"

vim.cmd.colorscheme(colorscheme)
