## Prerequisites:

**zsh-syntax-highlighting** - syntax highlighting for ZSH in standard repos <br>
**autojump** - jump to directories with j or jc for child or jo to open in file manager <br>
**zsh-autosuggestions** - Suggestions based on your history <br>

### To install:

**Manjaro:** <br>
`sudo pacman -Suy autojump zsh-autosuggestions zsh-syntax-highlighting` <br>

**macOS:** <br>
`brew install autojump zsh-autosuggestions zsh-syntax-highlighting` <br>

In the config file comment in Linux lines and comment out macos. <br>

**Files and directories locations:** <br>
**ZDOTDIR** set to $HOME/.config/zsh and configured in $HOME\.zshenv <br>
**zsh config folder** - $HOME/.config/zsh <br>
**history file** - $HOME/.cache/zsh/zsh-histfile <br>
**zcompdump file** - \$HOME/.cache/zsh/zcompdump-\$ZSH_VERSION <br>
**aliases file** - $HOME/.aliasrc <br>

**Theme:** <br>
In the .zshrc file the pure theme is configured to be installed by default. To disable it remove # Pure Prompt section. <br>
To disable displaying last commend times, comment in: prompt_pure_check_cmd_exec_time (around line 199) <br>

### neofetch (Depricated):

**Manjaro:** <br>
`sudo pacman -Suy neofetch / pamac install neofetch` <br>

**macOS:** <br>
`brew install neofetch`

### fastfetch:

**Manjaro** <br>
`sudo pacman -Suy fastfetch`

### Vi mode:

Vi mode is enabled in the .zshrc. To disable it comment out/remove # vi mode section and comment in # Emacs mode
