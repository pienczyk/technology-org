### What needs to be done for .vimrc to work: </br>

Create undo folder for persistent undo </br>
`mkdir -p $HOME/.cache/vim/undo`

Create directories for Plug </br>
`mkdir -p $HOME/.vim/{autoload,plugged}`
